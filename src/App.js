import { useState } from 'react';
import './App.css';

function App() {
  const [definitions, setDefinitions] = useState([]);
  const [word, setWord] = useState('');
  const ERRORMSG = "UNABLE TO FETCH THAT WORD";
  const [error,setError] = useState('');

  const getDefinitions = async () => {
    const url = 'https://api.dictionaryapi.dev/api/v2/entries/en/';
    let result;
    try {
      result = await fetch(url + word);
    } catch {
      setError(ERRORMSG);
      return;
    }
    if(result.status !== 200) {
      setError(ERRORMSG);
      return;
    }
    const parsedResult = await result.json();
    setDefinitions(parsedResult[0].meanings[0].definitions);
  }

  const handleInput = v => {
    setWord(v.target.value);
    setDefinitions([]);
    setError('');
  }

  console.log(definitions)
  return (
    <main>
      <input onChange={handleInput} value={word} placeholder="Enter word"></input>
      <button onClick={getDefinitions}>Lookup</button>
      {error && <p class="error">{error}</p>}
      <ul>
        {
          definitions.map((definition,i) => {
            
            return <li key={i}>{definition.definition}</li>
          })
        }
      </ul>
    </main>
  );
}

export default App;
